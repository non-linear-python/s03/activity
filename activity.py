from heapq import heapify, heappush, heappop, heapreplace, nlargest, nsmallest
min_heap = []
heappush(min_heap, 13)
heappush(min_heap, 30)
heappush(min_heap, 20)
heappush(min_heap, 40)
heappush(min_heap, 41)
heappush(min_heap, 51)
heappush(min_heap, 100)

def print_min_heap():
	print("Min-heap-elements:")
	for i in min_heap:
		print(i, end = ' ')
	print("\n")

print_min_heap()

print(f"largest values: {nlargest(3,min_heap)}")

element =heapreplace(min_heap, 35)
print('element deleted from heap '+str(element))

print_min_heap()

max_heap = []
heappush(max_heap, -13)
heappush(max_heap, -30)
heappush(max_heap, -20)
heappush(max_heap, -40)
heappush(max_heap, -41)
heappush(max_heap, -51)
heappush(max_heap, -100)

def print_max_heap():
	print ("Max-heap elements: ")
	for i in max_heap:
		print((-1*i), end=" ")
	print("\n")

print_max_heap()

print(f"2 largest: {nlargest(2,max_heap)}")
print(f"3 smallest: {nsmallest(3,max_heap)}")

max_heap.sort()
print_max_heap()